﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace csv_parser_reestr
{
    class Program
    {

        static void Main(string[] args)
        {
            string currentDir = Environment.CurrentDirectory;
            System.IO.TextReader readFile = new StreamReader(currentDir + "\\CSV EXCEL REESTR\\work test copy.csv");

            CSVreader reader = new CSVreader(readFile);

            int added_email_fields = 0;
            int added_web_fields = 0;
            //int added_telephone_fields = 0;
            ArrayList origin_fields = reader.GetFields;
            ArrayList new_fields = new ArrayList(origin_fields);

            const int origin_adress_id = 8;
            const int origin_tel_id = 9;
            const int origin_email_id = 10;
            const int origin_web_id = 11;

            new_fields.Insert(8, "index");
            new_fields.Insert(9, "city");
            //new_fields[10] = "yuridich_adress"
            //new_fields[11] = "telephone"
            //new_fields[12] = "email"
            //new_fields[13] = "sait"

            ArrayList new_rows = new ArrayList();

            for (int i = 0; i < reader.GetRowsCount; i++)
            {
                ArrayList origin_line = reader.GetRow(i);
                ArrayList new_line = new ArrayList();

                //добавим неизменяющиеся поля
                new_line.Add(origin_line[0]);
                for (int k = 1; k < 8; k++)
                {
                    new_line.Add(origin_line[k].ToString() + '"');
                }
                string origin_adress = origin_line[origin_adress_id].ToString();
                if (origin_adress == "\"0" || origin_adress == "#Н/Д")
                {
                    new_line.Add("#Н/Д"); //index
                    new_line.Add("#Н/Д"); //city
                    new_line.Add("#Н/Д"); //adress
                }
                else
                {

                    //выделим индекс
                    if (origin_adress[0] == '"')
                        origin_adress = origin_adress.Substring(1);
                    if (origin_adress[0] != '1')
                    {
                        //TODO: 8 исключительных ситуаций в строках: 547, 3414, 147, 3107, 3290, 3099, 3041, 493
                    }
                    string index = '"' + origin_adress.Substring(0, 6);
                    index += '"';
                    //добавим индекс в новую строку
                    new_line.Add(index);


                    origin_adress = origin_adress.Substring(6);
                    if (origin_adress[0] == ',')
                        origin_adress = origin_adress.Substring(1);
                    if (origin_adress[0] == ' ')
                        origin_adress = origin_adress.Substring(1);

                    //выделим город
                    string city = "\"";
                    if (origin_adress.Contains("г.Москва") ||
                        origin_adress.Contains("г. Москва") ||
                        origin_adress.Contains("город Москва") ||
                        origin_adress.Contains("Москва Город") ||
                        origin_adress.Contains("город Москва") ||
                        origin_adress.Contains("Москва")
                        )
                    {
                        city += "Москва";
                    }
                    else if (origin_adress.Contains("Московская область"))
                    {
                        city += "Москва";
                    }
                    else
                    {
                        city += "Москва";
                    }
                    city += '"';
                    //добавим город в новую строку
                    new_line.Add(city);

                    //Добавим оставшийся адрес
                    origin_adress = '"' + origin_adress.Substring(origin_adress.IndexOf(",") + 1);
                    origin_adress += '"';
                    new_line.Add(origin_adress);
                }

                //телефоны: (в таблице либо один телефон, либо "#Н/Д")
                new_line.Add(origin_line[origin_tel_id].ToString() + '"');
                

                //е-мэйлы:
                string origin_email_line = origin_line[origin_email_id].ToString();                
                if (origin_email_line[0] == '"')
                    origin_email_line = origin_email_line.Substring(1);

                if (origin_email_line.Contains(";") || origin_email_line.Contains("и") || origin_email_line.Contains("http:"))
                {
                    if (origin_email_line.Contains("http:"))
                    {
                        throw new Exception(":с");
                    }

                    int emails = 0;
                    while (origin_email_line != "")
                    {
                        string new_email = "error";
                        if (origin_email_line.Contains(";"))
                        {
                            new_email =  origin_email_line.Substring(0, origin_email_line.IndexOf(';'));
                        }
                        else if (origin_email_line.Contains("и"))
                        {
                            new_email = origin_email_line.Substring(0, origin_email_line.IndexOf('и'));
                        }
                        else
                        {
                            new_email = origin_email_line;
                        }

                        new_line.Add('"' + new_email + '"');
                        emails++;

                        
                        origin_email_line = origin_email_line.Substring(new_email.Length);
                        if (origin_email_line == "")
                            break;
                        if (origin_email_line[0] == ';' || origin_email_line[0] == 'и')
                            origin_email_line = origin_email_line.Substring(1);
                        if (origin_email_line == "")
                            break;
                        if (origin_email_line[0] == ' ')
                            origin_email_line = origin_email_line.Substring(1);
                    }
                    if (added_email_fields < emails)
                        added_email_fields = emails;
                }
                else
                {
                    new_line.Add('"' + origin_email_line + '"');
                }
                

                //сайты:
                string origin_sites_line = origin_line[origin_web_id].ToString();
                if (origin_sites_line[0] == '"')
                    origin_sites_line = origin_sites_line.Substring(1);

                if (origin_sites_line.Contains(";"))
                {
                    if (origin_sites_line.Contains("@"))
                        throw (new Exception(":c -> " + origin_line[0].ToString()));

                    int sites = 0;

                    while (origin_sites_line != "")
                    {
                        string new_site = "error";

                        if (origin_sites_line.Contains(";"))
                        {
                            new_site = origin_sites_line.Substring(0, origin_sites_line.IndexOf(';'));
                        }
                        else
                            new_site = origin_sites_line;

                        new_line.Add('"' + new_site + '"');
                        sites++;

                        origin_sites_line = origin_sites_line.Substring(new_site.Length);
                        if (origin_sites_line == "")
                            break;
                        if (origin_sites_line[0] == ';')
                            origin_sites_line = origin_sites_line.Substring(1);
                        if (origin_sites_line == "")
                            break;
                        if (origin_sites_line[0] == ' ')
                            origin_sites_line = origin_sites_line.Substring(1);
                    }
                    if (added_web_fields < sites)
                        added_web_fields = sites;
                }
                else
                {
                    new_line.Add('"' + origin_sites_line + '"');
                }

                //запишем результат в результирующий массив строк:
                new_rows.Add(new_line);
            }
            added_email_fields--; //потому что одно поле уже было
            added_web_fields--;



            //второй проход по new_rows, чтобы выровнять число полей по email и site

            for (int i = 0; i < new_rows.Count; i++)
            {
                ArrayList line = (ArrayList)new_rows[i];

                bool end_of_emails = false;
                int j = 0;

                while (!end_of_emails)
                {
                    if ((12 + j) >= line.Count)
                    {
                        end_of_emails = true;
                        break;                        
                    }
                    else if (!line[12 + j].ToString().Contains("@"))
                    {
                        end_of_emails = true;
                    }
                    else
                    {
                        j++;
                    }
                }
                int emails = j;
                while (emails < added_email_fields)
                {
                    line.Insert(12 + j, "\"\"");
                    emails++;
                }

                int web_start = 12 + emails; //всегда 21 
                int web_end = web_start;
                if (web_start == line.Count)
                {
                    web_end = web_start - 1;
                }
                else
                {
                    web_end = line.Count - 1;
                    bool no_mskobr = false;

                    if (!line[web_start].ToString().Contains("mskobr.ru"))
                    {
                        Object dub = line[web_start];
                        int k = web_start;
                        while (true)
                        {
                            if (k >= line.Count)
                            {
                                no_mskobr = true;
                                break;
                            }
                            else if (line[k].ToString().Contains("mskobr.ru"))
                            {
                                break;
                            }
                            else
                            {
                                k++;
                            }
                        }
                        if (!no_mskobr)
                        {
                            line[web_start] = line[k];
                            line[k] = dub;
                        }
                    }
                }
                int sites = web_end + 1 - web_start;

                while (sites < added_web_fields)
                {
                    line.Add("\"\"");
                    sites++;
                }

                int jj = 0;

            }

            //TODO: подправить заголовки

            int h_emails = 1;
            while (h_emails < added_email_fields)
            {
                new_fields.Insert(12 + h_emails, "email" + (h_emails+1));
                h_emails++;
            }
            int h_sites = 1;
            while (h_sites < added_web_fields)
            {
                new_fields.Add("site" + (h_sites + 1));
                h_sites++;
            }


            //вывод
            string header = "";
            for (int i = 0; i < new_fields.Count; i++)
            {
                header += new_fields[i].ToString() + ';';
            }

            System.IO.StreamWriter file = new System.IO.StreamWriter(currentDir + "\\CSV EXCEL REESTR\\testou.csv");
            file.WriteLine(header);

            for (int k = 0; k < new_rows.Count; k++)
            {
                string current_row = "";
                ArrayList current_line = (ArrayList)new_rows[k];
                for (int i = 0; i < new_fields.Count; i++)
                {
                    current_row += current_line[i].ToString() + ';';
                }
                file.WriteLine(current_row);
            }

            file.Close();

        }
    }
}

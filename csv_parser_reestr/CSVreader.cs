﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace csv_parser_reestr
{
    class CSVreader
    {
        public ArrayList _fields;
        private ArrayList _rows;

        public CSVreader()
        {
            return;
        }

        public CSVreader(TextReader textreader)
        {
            this.ReadHeaders(textreader);
            this.ReadRows(textreader);
        }

        public ArrayList GetFields
        {
            get { return _fields; }
        }

        public ArrayList GetRow(int i)
        {
            return (ArrayList)_rows[i];
        }

        public int GetRowsCount
        {
            get { return _rows.Count; }
        }

        private void ReadHeaders(TextReader textreader)
        {
            string row = textreader.ReadLine();
            ArrayList fields = new ArrayList();

            while (row.Length != 0)
            {
                string field_name = row.Substring(0, row.IndexOf(";"));
                row = row.Substring(field_name.Length + 1);
                fields.Add(field_name);
            }
            _fields = fields;
        }

        private void ReadRows(TextReader textreader)
        {

            ArrayList rows = new ArrayList();


            while (true)
            {
                ArrayList row = new ArrayList(_fields.Capacity);
                string line = textreader.ReadLine();
                if (line != null)
                {
                    for (int i = 0; i < _fields.Count; i++)
                    {
                        if (line == "")
                            break;
                        else
                        {
                            string row_field = "error";
                            if (line[0] == '"')
                            {
                                row_field = line.Substring(0, line.IndexOf("\";"));
                                line = line.Substring(row_field.Length + 2);
                            }
                            else
                            {
                                row_field = line.Substring(0, line.IndexOf(";"));
                                line = line.Substring(row_field.Length + 1);
                            }

                            row.Add(row_field);
                        }
                    }
                    rows.Add(row);
                }
                else
                    break;
            }
            _rows = rows;
        }


        public override string ToString()
        {
            string result = "error";

            result = "fields: " + _fields.Count + " rows: " + _rows.Count;

            return result;
        }

    }
}
